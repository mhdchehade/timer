extends CanvasLayer

var time = 0
var project = "default"
var project_i = 0
var keypath= "./keys" 
var public_key = "nokey"
var private_key = "nokey"

func _ready():
	load_key()
	
	updateTimer()
	get_time()
	output_project()
	
	$B_reset.modulate = Color(1.0, 0.3, 0.3, 1.0)

func _on_Button_pressed(): # when start button pressed
	output("timer start")
	get_time()
	$Timer.start()
	$E_project.visible = false

func _on_B_reset_pressed():
	output("data on currunt project is deleted")
	$Timer.stop()
	time = 0
	set_time(0)
	updateTimer()
	$E_project.visible = true
	get_time()

func _on_B_stop_pressed():
	output("timer stop")
	$Timer.stop()
	set_time(time)
	$E_project.visible = true
	get_time()

func _on_HTTPtimer_request_completed(result, response_code, headers, body):
	# called on function get_time()
	# get json data from server, 
	
	var json = JSON.parse(body.get_string_from_utf8())
	print (json.result)
	# {dreamlo:{leaderboard:{entry:[{date:12/9/2018 5:34:09 PM, name:timer, score:2, seconds:0, text:}, {date:12/9/2018 6:01:46 PM, name:default, score:1, seconds:0, text:}, {date:12/9/2018 5:51:35 PM, name:test1, score:1, seconds:0, text:}]}}}
	# {dreamlo:{leaderboard:{entry:{date:12/9/2018 5:22:54 PM, name:timer, score:1, seconds:0, text:}}}}
	if json.result.get("dreamlo").get("leaderboard") == null:
		output("no data on server")
		return
	if not typeof(json.result.get("dreamlo").get("leaderboard").get("entry")) == TYPE_ARRAY:
		output("no array on server")
		return
	
	var timeget = json.result.get("dreamlo").get("leaderboard").get("entry")
	var noprojectonserver = true
	for i in range(timeget.size()):
		
		projects(timeget[i].get("name"), timeget[i].get("score"))
		
		if timeget[i].get("name") == project:
			project_i = i
			noprojectonserver = false
	
	if noprojectonserver:
		output("project not found on server, setting it up")
		set_time(1)
	
	timeget = timeget[project_i].get("score")
	if timeget.is_valid_float():
		time = int(timeget)
		updateTimer()
	else:
		output("connection error")

func _on_Timer_timeout():
	time += 1 # + 1 min
	updateTimer()
	set_time(time)
	get_time()

func get_time():
	if public_key == "nokey": return
	$HTTPtimer.request("http://dreamlo.com/lb/" + public_key + "/json")

func set_time(stime):
	if private_key == "nokey": return
	if stime == 0:
		$HTTPRequest.request("http://dreamlo.com/lb/" + private_key + "/delete/" + project + "/")
	else:
		$HTTPRequest.request("http://dreamlo.com/lb/" + private_key + "/add/" + project + "/" + String(stime))

func output(output): # function to output messages on gui
	$L_output.text = String(output)

func output_list_names(output):
	$List_names.text = String(output)

func output_list_numbers(output):
	$List_numbers.text = String(output)

func updateTimer():
	if time < 60:
		$L_time.text = "total time on " + project + ":  " + String(time) + " mins"
	else:
		$L_time.text = "total time on " + project + ":  " + String(int(time / 60)) + " hours"

func _on_E_project_text_entered(new_text):
	time = 0
	project = new_text
	get_time()
	updateTimer()
	output_project()
	output("new project selected: " + project)

var projects = []
func projects(p_name, p_time): # function for desplaying active projects
	
	if projects.find([p_name, p_time]) == -1:
		var array = [p_name, p_time]
		projects.append(array)
	
	var x = 8
	x = min(x, projects.size())
	
	var text_names = ""
	var text_numbers = ""
	for i in x:
		text_names += String(projects[i][0]) + "\n"
		text_numbers += String(float(projects[i][1]) / 60.0) + "\n"
	
	output_list_numbers(text_numbers)
	output_list_names(text_names)

func output_project(): # display the name of the current project
	$Project.text = project


func load_key():
	var configFile= ConfigFile.new() 
	# Load file 
	configFile.load(keypath) 
	
	# Check if "public_key" exists in keys section 
	if configFile.has_section_key("keys", "public_key") and configFile.has_section_key("keys", "private_key"): 
		public_key = configFile.get_value("keys", "public_key") 
		private_key = configFile.get_value("keys", "private_key") 
	else:
		output("no key where found! please set up the key in the appropriate folder")

